import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { LoadingService } from '../../loading/loading.service';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(
    private loadingServices: LoadingService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.includes('myrxpal.com')){
      this.loadingServices.present();
    }
    return next.handle(req).pipe(
      catchError(this.getError),
      finalize(async () => {
        setTimeout(async () => {
          await this.loadingServices.dismiss();
        }, 500);
      })
    );
  }

  getError( error: HttpErrorResponse ){
    let msg = '';
    switch (error.status) {
      case 0: msg = 'Contection Error'; console.log('Connection Error', 'Error en la conexion'); break;
      case 400: msg = 'Not Found'; console.log('Not Found', 'Ruta no encontrada'); break;
      case 500: msg = 'Server Error'; console.log('Server Error', 'Error en el servidor'); break;
      case 504: msg = 'Error TimeOut'; console.log('Error TimeOut', 'Error en tiempo de respuesta'); break;
      case 598: msg = 'Error TimeOut'; console.log('Error TimeOut', 'Error en tiempo de respuesta'); break;
      default: msg = error.statusText; console.log(error.statusText, error.status); break;
    }
    return throwError(msg);
  }

}
