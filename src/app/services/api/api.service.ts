import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';
import { Observable } from 'rxjs';
import { IRespOtp, IRespSearch, IResp } from '../../interfaces/interfaces';
import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  domain = environment.domain;

  constructor(
    private http: HttpClient
  ) { }

  getSearch(fields): Observable<IRespSearch[]>{
    return this.http.post<IRespSearch[]>(this.domain + '/search/getSearch', fields);
  }

  sendOtp(fields): Observable<IRespOtp>{
    return this.http.post<IRespOtp>(this.domain + '/api/sendOTP', fields );
  }

  validateOtpEmail(fields): Observable<IRespOtp>{
    return this.http.post<IRespOtp>(this.domain + '/api/validateOTP', fields );
  }

  validateOtpMobile(fields): Observable<IRespOtp>{
    return this.http.post<IRespOtp>(this.domain + '/api/validateOTP_mobile', fields );
  }

  register(fields): Observable<IResp>{
    return this.http.post<IResp>(this.domain + '/api/Register', fields);
  }

  login(fields): Observable<IResp>{
    return this.http.post<IResp>(this.domain + '/api/checkLogin_jwt', fields);
  }
}
