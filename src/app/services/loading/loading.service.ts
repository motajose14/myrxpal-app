import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  loading = null;

  constructor(
    private loadinCtrl: LoadingController
  ) { }

  async present(){
    this.loading = await this.loadinCtrl.create({message: 'Connecting'});
    await this.loading.present();
  }

  async dismiss(){
    await this.loading.dismiss();
  }
}
