import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';


@Injectable({
  providedIn: 'root'
})
export class MessageService {


  constructor() {
  }

  async present(msg){
    console.log(msg);
    const { Toast } = Plugins;
    await Toast.show({text: msg, duration: 'long', position: 'bottom'});
  }
}
