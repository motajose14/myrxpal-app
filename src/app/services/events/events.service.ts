import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  events = new Subject<string>();
  listenEvents = this.events.asObservable();

  constructor() { }

  sendEvents(event: string){
    this.events.next(event);
  }
}
