import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Plugins } from '@capacitor/core';
import { ModalController } from '@ionic/angular';
import { ApiService } from '../../services/api/api.service';
import { SearchComponent } from '../../shared/modal/search/search.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  filter = 'All';
  searchTerm = '';

  constructor(
    private api: ApiService,
    private router: Router,
    private modalCtrl: ModalController
  ) {}


  async getSearch(ev){
    await (await this.modalCtrl.create({
      component: SearchComponent,
      componentProps: {searchTerm: ev.searchTerm, filter: ev.filter}})
      ).present();
  }

}
