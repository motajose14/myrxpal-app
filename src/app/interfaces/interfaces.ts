export interface IRespSearch {
    stype?: 'medication' | 'pharmacy';
    id?: string;
    pharmacy_name?: string;
    dnd?: string;
    commonname?: string;
}

export interface IRespOtp {
    result?: boolean;
    msg?: string;
}

export interface IResp {
    result?: boolean;
    msg?: string;
}
