import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent implements OnInit {

  @Input() searchTerm = '';
  @Input() filter = 'All';
  @Output() search: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {}

  getSearch(){
    const resp = {searchTerm: this.searchTerm, filter: this.filter};
    this.search.emit(resp);
  }

  changeFilter(event){
    this.filter = event.detail.value;
  }

}
