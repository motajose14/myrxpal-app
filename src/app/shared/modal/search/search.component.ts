import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ApiService } from '../../../services/api/api.service';
import { IRespSearch } from '../../../interfaces/interfaces';
import { MessageService } from '../../../services/message/message.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {

  searchTerm: string;
  filter: string;
  results: IRespSearch[] = [];

  constructor(
    public modalCtrl: ModalController,
    private api: ApiService,
    private messageService: MessageService
    ) { }

  async ngOnInit() {
    await this.getSearch();
  }

  async getSearch(){
    await this.api.getSearch({searchTerm: this.searchTerm}).subscribe((results) => {
      this.results = results;
    }, async (err) => {
      await this.messageService.present(err);
    });
  }

  async search(ev){
    this.searchTerm = ev.searchTerm;
    this.filter = ev.filter;
    await this.getSearch();
  }

}
