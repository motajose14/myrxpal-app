import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { NotificationsComponent } from '../popover/notifications/notifications.component';
import { ShoppingCartComponent } from '../popover/shopping-cart/shopping-cart.component';
import { EventsService } from '../../services/events/events.service';
import { StorageService } from '../../services/storage/storage.service';
import { ActivationEnd, Router } from '@angular/router';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  menus = [
    {title: 'Home', icon: 'link', url: '/'},
    {title: 'How it works', icon: 'link', url: '/'},
    {title: 'Recently Viewed', icon: 'link', url: '/'},
    {title: 'Blog', icon: 'blog', url: '/pages/blog'},
    {title: 'Chat', icon: 'sms', url: '/'},
    {title: 'Help', icon: 'question', url: '/'},
  ];
  logged = false;
  title: string;
  headerShow = true;

  constructor(
    private popover: PopoverController,
    private eventsServices: EventsService,
    private storage: StorageService,
    private router: Router
  ) {
    this.getDataRouter().subscribe((data) => {
     this.title = data.title;
    });

    this.getUrlRouter().subscribe((data) => {
      if (data === 'blog'){
        this.headerShow = false;
      } else {
        this.headerShow = true;
      }
    });
  }

  async ngOnInit() {
    await this.getLogged();
    this.eventsServices.listenEvents.subscribe((dataEvent) => {
      switch (dataEvent) {
        case 'logged': this.logged = true; break;
        default: console.log('Unknown event'); break;
      }
    });
  }

  async getLogged(){
    const token = await this.storage.getItem('token');
    if (token !== null && token !== ''){
      this.logged = true;
    } else {
      this.logged = false;
    }
  }

  async openNotification(e){
    const pop = await this.popover.create({
      component: NotificationsComponent,
      event: e,
      translucent: true
    });
    return await pop.present();
  }

  async openShopping(e){
    const pop = await this.popover.create({
      component: ShoppingCartComponent,
      event: e,
      translucent: true
    });
    return await pop.present();
  }

  async logout(){
    await this.storage.removeItem('token');
    this.logged = false;
  }

  getDataRouter(){
    return  this.router.events.pipe(
      filter(ev => ev instanceof ActivationEnd),
      filter((ev: ActivationEnd) => ev.snapshot.firstChild === null),
      map((ev: ActivationEnd) => ev.snapshot.data )
    );
  }

  getUrlRouter(){
    return  this.router.events.pipe(
      filter(ev => ev instanceof ActivationEnd),
      filter((ev: ActivationEnd) => ev.snapshot.firstChild === null),
      map((ev: ActivationEnd) => ev.snapshot.routeConfig.path )
    );
  }

}
