import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MyValidators } from '../../validators/myvalidators.validator';
import { ValidatorMessages } from '../../validators/messages';
import { ApiService } from '../../services/api/api.service';
import { MessageService } from '../../services/message/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {


  formRegister = new FormGroup({
    first_name: new FormControl(null, Validators.compose([Validators.required, Validators.minLength(3), Validators.pattern('[a-zA-Z ]*')])),
    last_name: new FormControl(null, Validators.compose([Validators.required, Validators.minLength(3), Validators.pattern('[a-zA-Z ]*')])),
    userlink: new FormControl(null, Validators.compose([Validators.required, MyValidators.emailOrMobile])),
    password1: new FormControl(null, Validators.compose([Validators.required, Validators.pattern('(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,16})$')])),
    offerpromotion: new FormControl(false),
    otp: new FormControl(null),
    ltz: new FormControl(Intl.DateTimeFormat().resolvedOptions().timeZone),
    otptype: new FormControl(null)
  });

  disabledOtp = false;
  validOtp = false;
  time = null;

  validationMessages = ValidatorMessages;

  constructor(
    private api: ApiService,
    private messageService: MessageService,
    private router: Router
  ) { }

  ngOnInit() {}

  sendOtp(){
    if (this.formRegister.get('userlink').valid){
      const form = {
        userlink : this.formRegister.get('userlink').value,
        option: 'register',
        otptype: (isNaN(Number(this.formRegister.get('userlink').value)) ? 'email' : 'mobile')
      };
      this.api.sendOtp(form).subscribe( async (data) => {
        if (data.result) {
          this.disabledOtp = true;
        }
        await this.messageService.present(data.msg);
      }, async (err) => {
        await this.messageService.present(err);
      });
    }
  }

  validateOtp(){
    if ( String(this.formRegister.get('otp').value).length >= 4 && this.formRegister.get('userlink').valid){
      clearTimeout(this.time);
      let form = null;
      this.time = setTimeout(() => {
        if (isNaN(Number(this.formRegister.get('userlink').value))){
          form = {
            userlink: this.formRegister.get('userlink').value,
            otp: this.formRegister.get('otp').value,
            otptype: 'email'
          };
          this.api.validateOtpEmail(form).subscribe( async (data) => {
            this.validOtp = data.result;
            await this.messageService.present(data.msg);
          }, async (err) => {
            await this.messageService.present(err);
          });
        } else {
          form = {
            mobile: this.formRegister.get('userlink').value,
            code: this.formRegister.get('otp').value
          };
          this.api.validateOtpMobile(form).subscribe( async (data) => {
            this.validOtp = data.result;
            await this.messageService.present(data.msg);
          }, async (err) => {
            await this.messageService.present(err);
          });
        }
      }, 1000);
    }
  }

  sendRegister(){
    this.formRegister.get('otptype').setValue((isNaN(Number(this.formRegister.get('userlink').value)) ? 'email' : 'mobile'));
    this.api.register(this.formRegister.value).subscribe( async (data) => {
      if (data.result){
        setTimeout(() => {
          this.router.navigate(['user/sign-in']);
        }, 500);
        await this.messageService.present(data.msg);
      }
    }, async (err) => {
      await this.messageService.present(err);
    });
  }

}
