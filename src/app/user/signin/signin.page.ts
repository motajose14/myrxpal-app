import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MyValidators } from '../../validators/myvalidators.validator';
import { ValidatorMessages } from '../../validators/messages';
import { ApiService } from '../../services/api/api.service';
import { MessageService } from '../../services/message/message.service';
import { StorageService } from '../../services/storage/storage.service';
import { Router } from '@angular/router';
import { EventsService } from '../../services/events/events.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.page.html',
  styleUrls: ['./signin.page.scss'],
})
export class SigninPage implements OnInit {

  formLogin = new FormGroup({
    userlink: new FormControl(null, Validators.compose([Validators.required, MyValidators.emailOrMobile])),
    password1: new FormControl(null, [ Validators.required]),
    ltz: new FormControl(Intl.DateTimeFormat().resolvedOptions().timeZone),
    otp: new FormControl(null)
  });

  disabledOtp = false;
  validOtp = false;
  time = null;

  validationMessages = ValidatorMessages;

  constructor(
    private api: ApiService,
    private messageService: MessageService,
    private storage: StorageService,
    private router: Router,
    private eventsServices: EventsService
  ) { }

  ngOnInit() {
  }

  sendOtp(){
    if (this.formLogin.get('userlink').valid){
      const form = {
        userlink : this.formLogin.get('userlink').value,
        option: 'login',
        otptype: (isNaN(Number(this.formLogin.get('userlink').value)) ? 'email' : 'mobile')
      };
      this.api.sendOtp(form).subscribe( async (data) => {
        if (data.result) {
          this.disabledOtp = true;
        }
        await this.messageService.present(data.msg);
      }, async (err) => {
        await this.messageService.present(err);
      });
    }
  }

  validateOtp(){
    if ( String(this.formLogin.get('otp').value).length >= 4){
      clearTimeout(this.time);
      let form = null;
      this.time = setTimeout(() => {
        if (isNaN(Number(this.formLogin.get('userlink').value))){
          form = {
            userlink: this.formLogin.get('userlink').value,
            otp: this.formLogin.get('otp').value,
            otptype: 'email'
          };
          this.api.validateOtpEmail(form).subscribe( async (data) => {
            this.validOtp = data.result;
            await this.messageService.present(data.msg);
          }, async (err) => {
            await this.messageService.present(err);
          });
        } else {
          form = {
            mobile: this.formLogin.get('userlink').value,
            code: this.formLogin.get('otp').value
          };
          this.api.validateOtpMobile(form).subscribe( async (data) => {
            this.validOtp = data.result;
            await this.messageService.present(data.msg);
          }, async (err) => {
            await this.messageService.present(err);
          });
        }
      }, 3000);
    }
  }

  sendLogin(){
    const fields = {
      username: this.formLogin.get('userlink').value,
      pwd: this.formLogin.get('password1').value,
      ltz: this.formLogin.get('ltz').value
    };

    this.api.login(fields).subscribe(async (data) => {
      if (data.result){
        await this.storage.setItem('token', data.msg);
        await this.messageService.present('Welcome');
        this.eventsServices.sendEvents('logged');
        this.router.navigate(['/']);
      } else {
        await this.messageService.present(data.msg);
      }
    }, async (err) => {
      await this.messageService.present(err);
    });
  }

}
