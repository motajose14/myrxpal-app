import { FormControl } from '@angular/forms';

export class MyValidators {

    static emailOrMobile(fc: FormControl){
        let pattern = null;
        if (fc.value === '' || fc.value === 0 || fc.value === null){
            return (null);
        } else if (isNaN(Number(fc.value))) {
            pattern = new RegExp(/[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/);
            return (pattern.test(fc.value) ? null : {email: true});
        } else if (!isNaN(Number(fc.value))) {
            pattern = new RegExp('^[0-9]{2,3}-? ?[0-9]{6,7}$');
            return (pattern.test(String(fc.value)) ? null : {mobile: true});
        }
      }

}
