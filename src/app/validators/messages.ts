export const ValidatorMessages = {
    first_name: [
        { type: 'required', message: 'First name is required.' },
        { type: 'minlength', message: 'First name must be at least 3 characters long.' },
        { type: 'pattern', message: 'Your first name must contain only letters' }
      ],
    last_name: [
        { type: 'required', message: 'Last name is required.' },
        { type: 'minlength', message: 'Last name must be at least 3 characters long.' },
        { type: 'pattern', message: 'Your last name must contain only letters' }
      ],
    userlink: [
        { type: 'required', message: 'Email or Mobile is required.' },
        { type: 'email', message: 'Email invalid.' },
        { type: 'mobile', message: 'Mobile invalid.' }
      ],
    password1: [
      { type: 'required', message: 'Password is required.' },
      { type: 'pattern', message: '(Between 8 and 16 characters, at least one digit and one alphanumeric, and cannot contain space characters)' }
    ]
  };
